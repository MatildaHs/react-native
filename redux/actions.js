import { SET_TITLE, SET_AMOUNT } from "./types";

export const setTitle = name => dispatch => {
    dispatch({
        type: SET_TITLE,
        payload: title,
    });
}
export const setAmount = amount => dispatch => {
    dispatch({
        type: SET_AMOUNT,
        payload: amount,
    });
}