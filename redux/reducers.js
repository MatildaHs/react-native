import { SET_TITLE, SET_AMOUNT } from "./types";

const initialState = {
    title: '',
    amount: '',
}

function useReducer(state = initialState, action) {
    switch (action.type) {
        case SET_TITLE:
            return { ...state, title: action.payload }
        case SET_AMOUNT:
            return { ...state, amount: action.payload }
        default:
            return state;
    }
}
export default useReducer;