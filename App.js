import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator }
  from '@react-navigation/native-stack'
import { initDB, getTableInfo }
  from './database/DbUtils'
import { useEffect } from 'react';
import MainShoppingScreen from './screens/MainShoppingScreen';
import SelectedShoppingScreen from './screens/SelectedShoppingScreen';
import StartingScreen from './screens/StartingScreen'
import { Provider } from 'redux';
import { Store } from './redux/store';

export default function App() {

  useEffect(() => {
    initDB()
      .then(res => {
        console.log("result from init", res)
        return getTableInfo()
      })
      .then(res => console.log("pragma table_info", res))
      .catch(err => console.log(err))
  }, [])

  const NativeStack = createNativeStackNavigator()

  return (


    <NavigationContainer>
      <NativeStack.Navigator>
        <NativeStack.Screen
          options={{ headerShown: false }}
          name='StartingScreen'
          component={StartingScreen}

        />
        <NativeStack.Screen
          name='MainShoppingScreen'
          component={MainShoppingScreen}
          options={{
            title: 'Create List',
            headerStyle: {
              backgroundColor: "transparent",//'#dee2df',
              opacity: 0.6
            },
            headerTitleStyle: {
              fontWeight: "bold",
              fontSize: 25
            },
          }}
        />
        <NativeStack.Screen
          options={{
            title: 'Product Description',
            headerTitleStyle: {
              fontWeight: "bold",
              fontSize: 25
            },
          }}
          name='SelectedShoppingScreen'
          component={SelectedShoppingScreen}
        />
      </NativeStack.Navigator>
    </NavigationContainer>

  )


}