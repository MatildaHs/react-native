import { Text, TextInput, StyleSheet, View, Button, TouchableOpacity, TouchableHighlight, Pressable } from 'react-native'
import { useState, useEffect } from 'react';
import Product from '../models/Product'
import { findAll, insert } from '../database/DbUtils'
import React, { Component } from 'react'


const ProductInput = ({ setProducts }) => {

    const [textInputValue, setTextInputValue] = useState('')

    const handleTextChange = (text) => {
        setTextInputValue(text)
    }

    const [amountInputValue, setAmountInputValue] = useState('')

    const handleAmountChange = (text) => {
        setAmountInputValue(text)
    }

    const handleAdd = () => {
        const product = new Product(0, textInputValue, amountInputValue, false, )

        insert(product)
            .then(res => {
                console.log("insert res", res)
                return findAll()

            })
            .then(res => setProducts(res))
            .catch(err => console.log("Vad gick fel? ", err))
        setTextInputValue('');
        setAmountInputValue('');
    }

    return (
        <View accessible={true} accessibilityLabel='Product input region' style={styles.inputcontainer}>
            <TextInput
                style={styles.textinput}
                onChangeText={handleTextChange}
                value={textInputValue}
            />
            <TextInput
                style={styles.amountinput}
                onChangeText={handleAmountChange}
                value={amountInputValue}
            />
            <Pressable
                onPress={handleAdd}
                style={({ pressed }) => [styles.addbutton, { opacity: 1.0 }]}
            >
                <Text style={styles.buttontext}>Add</Text>
            </Pressable>
        </View>
    )
}


const styles = StyleSheet.create({
    inputcontainer: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: "center",
        marginVertical: 40,
        flex: 1,
        height: "50%"


        // marginBottom: "80%"
    },
    addbutton: {
        backgroundColor: 'rgba(22, 21, 22, 0.9)',
        padding: 10,
        margin: 20,
        height: 60,
        borderRadius: 10,
        width: "80%",

    },
    amountinput: {
        width: "90%",
        paddingHorizontal: 10,
        height: 60,
        margin: 10,
        borderRadius: 6,
        backgroundColor: '#FFF',

    },
    textinput: {
        width: "90%",
        backgroundColor: '#FFF',
        margin: 10,
        paddingHorizontal: 10,
        height: 60,
        borderRadius: 6,

    },
    buttontext: {
        color: '#FFF',
        fontSize: 25,
        textAlign: "center",
        fontWeight: "bold"
    }


});

export default ProductInput;



{/* <Pressable
onPress={() => { toggleCheckMark(); }}
style={({ pressed }) => [styles.addbutton, { opacity: 1.0 }]}

>  */}




{/* <Pressable
style={styles.amountinput}
onPress={handleAmountChange} >
<Text> <AntDesign name="up" size={30} color="black" /></Text>
</Pressable> */}