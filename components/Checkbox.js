import { useState } from "react";
import React from 'react';
import { Pressable, Text, StyleSheet, View } from "react-native";
import { MaterialCommunityIcons } from '@expo/vector-icons';

const Checkbox = (props) => {


    const iconName = props.isChecked ?
        "checkbox-marked" : "checkbox-blank-outline";



    const [checked, setCheckMark] = useState(false);

    return (
        <View style={styles.container}>
            <Pressable onPress={props.onPress}>
                <MaterialCommunityIcons
                    name={iconName} size={24} color="black" />
            </Pressable>
            <Text style={styles.title}>{props.title}</Text>
        </View>
    );
};


export default Checkbox;


const styles = StyleSheet.create({
    container: {
        justifyContent: "flex-end",
        alignSelf: "flex-end",
        flexDirection: "row",
        flex: 1,

        width: 150,
        marginTop: 5,
        zIndex: 100,
        marginHorizontal: 5,
    },
    title: {
        fontSize: 16,
        color: "#000",
        marginLeft: 5,
        fontWeight: "600",
    },
});
{/* <Text style={styles.title}>{checked ? '"checkbox-marked"' : "checkbox-blank-outline"} </Text>  <Pressable onPress={props.onPress}>*/ }