import { StyleSheet, Text } from 'react-native';

const Header = () => {
    return (
        <Text style={styles.title}>Create your Shoppinglist!</Text>
    )
}

const styles = StyleSheet.create({
    title: {
        fontSize: 30,
        marginTop: 40,
        padding: 10,

        textAlign: 'center',

        color: '#ffffff',

    }
})

export default Header;
