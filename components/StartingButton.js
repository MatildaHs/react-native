import { useNavigation } from '@react-navigation/native'
import { Text, View, ImageBackground, StatusBar, StyleSheet, Dimensions, NativeEventEmitter, Pressable } from 'react-native'
import { useState, useEffect } from 'react';
import Header from '../components/Header'
import { AntDesign } from '@expo/vector-icons';


import ProductInput from '../components/ProductInput';
import ShoppingList from '../components/ShoppingList';

import { findAll, dropTable } from '../database/DbUtils'
const StartingButton = ({ navigation }) => {

    const handlePress = () => {

        navigation.navigate('MainShoppingScreen')
    }




    return (

        <Pressable
            onPress={() => { handlePress() }}
            style={({ pressed }) =>
                [styles.startbutton, { opacity: pressed ? 0.75 : 0.9 }]}
        >
            <Text style={styles.buttontitle}>  Lists          <AntDesign name="doubleright" size={35} color="white" /></Text>
        </Pressable>

    )
}

const styles = StyleSheet.create({

    startbutton: {
        backgroundColor: '#3acb8d',
        width: "70%",
        height: 80,
        padding: 15,
        //marginVertical: "50%",
        alignSelf: "center",
        borderRadius: 20,
        justifyContent: "center"
    },
    buttontitle: {
        fontSize: 40,

        color: '#FFF'
    }
});
export default StartingButton;