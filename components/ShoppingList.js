import { useNavigation } from '@react-navigation/native'
import { Text, FlatList, Pressable, StyleSheet } from 'react-native'
import Checkbox from '../components/Checkbox'
import { useState } from 'react';
import { findAll, insert, updateCompleted } from '../database/DbUtils'


const ShoppingList = ({ products, navigation }) => {


    const [checked, setCheckMark] = useState(false);

    const toggleCheckMark = () => {
        setCheckMark(!checked)
    }

    const handlePress = (product) => {
        navigation.navigate('SelectedShoppingScreen', { product: product })
    }

    const _renderItem = ({ item: product, id: id, amount: amount }) => {
        return (
            <Pressable
                onPress={() => handlePress(product)}
                style={styles.container}
            >
                <Text style={styles.product}>{product.id} {product.title} {product.amount}</Text>

            </Pressable>
        )
    }

    return (
        <FlatList
            style={[styles.listing]}
            data={products}
            renderItem={_renderItem}
            keyExtractor={(product, index) => index}
        />
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 2,
        //padding: 10,
        flexDirection: "row",
        width: "85%",
        alignItems: "center",
        height: 50
        //textAlign: "center",
        //justifyContent: "center"

    },
    listing: {
        backgroundColor: "rgba(23, 22, 23, 0.4)",
        margin: 20,


        flex: 3,
        //alignSelf: "center"
    },
    product: {

        //alignItems: "center",
        backgroundColor: '#FFF',
        borderBottomColor: "black",
        padding: 10,
        opacity: 1.0,
        width: "90%",
        zIndex: 30,
        fontSize: 18,
        borderBottomWidth: 1,
    },


})
{/* <Checkbox
                    onPress={() => setCheckMark(!checked)}
                    title="Done"
                    isChecked={checked}
                /> */}
export default ShoppingList;
{/*              <Checkbox onPress={() => { setCheckMark(!checked) }}

                    isChecked={checked}
                /></Text> */}