import * as SQLite from 'expo-sqlite';
import Product from '../models/Product'
import { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { setTitle, setAmount } from '../redux/actions';

const db = SQLite.openDatabase("productsdb.db");


export const initDB = () => {

    /*     const {title, amount}=useSelector(state=>state.userReduces);
        const dispatch=useDispatch();
        
    
        useEffect(()=>{
            createTable();
            getData();
        }, []);
    
    
        const createTable =()=>{}
    
        const getData=()=>{}
    
    
    const setData=async ()=>{
        if(title.length==0 || amount.length==0)Alert.alert('Warning!', "Enter details")
        else{
            try{
                dispatch(setTitle(title));
                dispatch(setAmount(amount));
                await db.transaction(async(tx)=>{
                    await tx.executeSql("INSERT INTO products (Title, Amount) VALUES(?,?)", [title,amount]);
                })
                
            }catch(error){console.log(error);}
        }
    }
    return <View>
        <TextInput style={styles.textinput} placeholder='Product' onChangeText={(value)=> dispatch(setTitle(value))}/>
        <TextInput style={styles.textinput} placeholder='Amount' onChangeText={(value)=> dispatch(setAmount(value))}/>
        <StartingButton title='List' pressFunction={setData}/>
    </View>
    
    
    const styles = StyleSheet.create({
        textinput: {
            width: "90%",
            backgroundColor: '#FFF',
            margin: 10,
            paddingHorizontal: 10,
            height: 60,
            borderRadius: 6,
    
        }
    }) 

    return new Promise((resolve, reject) => {

        db.transaction((transaction) => {

            transaction.executeSql(
                `CREATE TABLE IF NOT EXISTS products (
                       id INTEGER PRIMARY KEY NOT NULL,
                       title TEXT NOT NULL,
                       
                       completed BOOLEAN NOT NULL
                   )`, [],
                (tx, res) => resolve(res),
                (tx, err) => reject(err)
            )

        })
    })
}
 */
    return new Promise((resolve, reject) => {

        db.transaction((transaction) => {

            transaction.executeSql(
                `DROP TABLE IF EXISTS products`, [],
                (tx, res) => resolve(res),
                (tx, err) => reject(err)
            )

        })
    })

}

export const getTableInfo = () => {

    return new Promise((resolve, reject) => {

        db.transaction((transaction) => {

            transaction.executeSql(
                `pragma table_info('products')`, [],
                (tx, res) => resolve(res),
                (tx, err) => reject(err)
            )

        })
    })

}

export const insert = (product) => {

    return new Promise((resolve, reject) => {

        db.transaction((transaction) => {
            transaction.executeSql(
                `INSERT INTO products (title, amount, completed)
                VALUES (?, ?,?)`, [product.title, product.amount, product.isCompleted],
                (tx, res) => resolve(res),
                (tx, err) => reject(err)
            )
        })

    })
}

export const findAll = () => {

    return new Promise((resolve, reject) => {

        db.transaction((transaction) => {
            transaction.executeSql(
                `SELECT * FROM products`, [],
                (tx, res) => resolve(
                    res.rows._array
                        .map(product => new Product(product.id, product.title, product.amount, product.completed === 1))
                ),
                (tx, err) => reject(err)
            )
        })

    })
}
export const updateCompleted = (id) => {
    return new Promise((resolve, reject) => {

        db.transaction((transaction) => {
            transaction.executeSql(
                ` UPDATE todos SET completed=? WHERE id=?`, [id],
                (tx, results) => {
                    console.log('Results', results.rowsAffected);
                    if (results.rowsAffected > 0) {
                        Alert.alert('Record Updated Successfully...')
                    } else Alert.alert('Error');
                }
            )
        })

    })
}


export const deleteById = (id) => {

    return new Promise((resolve, reject) => {

        db.transaction((transaction) => {
            transaction.executeSql(
                `DELETE FROM products WHERE id = ?`, [id],
                (tx, res) => resolve(res),
                (tx, err) => reject(err)
            )
        })

    })

}
