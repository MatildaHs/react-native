import { Text, View, TextInput, ImageBackground, StatusBar, Button, StyleSheet, Dimensions, NativeEventEmitter } from 'react-native'
import Header from '../components/Header';
import ProductInput from '../components/ProductInput';
import ShoppingList from '../components/ShoppingList';
import { useState, useEffect } from 'react';
import { findAll } from '../database/DbUtils'
import { setTitle, setAmount } from '../redux/actions';



const MainShoppingScreen = ({ navigation }) => {

    const [products, setProducts] = useState([])



    return (
        <View style={styles.container}>
            <ImageBackground
                source={require('../assets/list.jpg')}
                resizeMode='cover'
                style={styles.imagebackground}
            >
                <Header />



                <ShoppingList
                    products={products}
                    navigation={navigation}
                />

            </ImageBackground>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,

    },
    imagebackground: {
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width
    },
    checkboxLabel: {
        marginLeft: 8,

        fontSize: 18,
    },
    textinput: {
        width: "90%",
        backgroundColor: '#FFF',
        margin: 10,
        paddingHorizontal: 10,
        height: 60,
        borderRadius: 6,

    }
});

export default MainShoppingScreen;


/* 

    const { title, amount } = useSelector(state => state.userReduces);
    const dispatch = useDispatch();


    const findAll = () => {

        try {

            db.transaction((transaction) => {
                transaction.executeSql(
                    `SELECT Title, Amount FROM products`, [],
                    (tx, res) => {
                        let len = res.rows.length;
                        if (len > 0) {
                            let titleName = res.rows.item(0).Title;
                            let amountValue = res.rows.item(0).Amount;
                            dispatch(setTitle(titleName));
                            dispatch(setAmount(amountValue));
                        }

                    },
                    (tx, err) => reject(err)
                )
            })


        }
        catch (error) { console.log(error) };
    }
*/