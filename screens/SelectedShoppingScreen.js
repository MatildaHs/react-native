import { TouchableOpacity, NativeEventEmitter, Pressable, StyleSheet, Text, View, Dimensions, ImageBackground } from "react-native"
import { deleteById, updateCompleted } from "../database/DbUtils"
import Checkbox from '../components/Checkbox'
import { useState, useEffect } from 'react';

const SelectedShoppingScreen = ({ route, navigation }) => {

    const { id, product, amount, completed } = route.params

    const emitter = new NativeEventEmitter()


    const [checked, setCheckMark] = useState(false);


    const toggleCheckMark = () => {
        setCheckMark(!checked)
    }
    const deleteItemById = (id) => {
        deleteById(id)
            .then(res => emitter.emit('delete', id, product, amount, checked))

        navigation.goBack()
    }


    return (
        <View style={styles.container}>
            <ImageBackground
                source={require('../assets/list.jpg')}
                resizeMode='cover'
                style={styles.imagebackground}
            >
                <View style={styles.textcontainer}>
                    <Text style={[{ color: "white", fontSize: 35 }]}>Title: {product.title}</Text>
                    <Text style={[{ color: "white", fontSize: 35 }]}>Amount: {product.amount}</Text>
                    <Text style={[{ color: "white", fontSize: 35 }]}>Id: {product.id}</Text>
                    <TouchableOpacity
                        onPress={() => { toggleCheckMark(); }}>
                        <Text style={styles.title}>{checked ? 'DONE' : "NOT DONE"} </Text>
                    </TouchableOpacity>
                    <Pressable style={[{ color: "white", fontSize: 35 }]} onPress={() => deleteItemById(product.id)}>
                        <Text style={[{ color: "white", fontSize: 35 }]}>Delete</Text>
                    </Pressable>

                </View>
            </ImageBackground>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        color: "white",
    },
    imagebackground: {
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width
    },
    textcontainer: {
        color: "white",
        flex: 1,
        backgroundColor: "rgba(0, 0, 0, 0.6)",
        justifyContent: "center",
        alignItems: "center",
    },
    title: {
        color: "white",
        fontSize: 20
    },


})

{/*      <Checkbox
                        onPress={() => setCheckMark(!checked)}
                        title="Music"
                        isChecked={checked}
                    />
                
                const toggleCheckMark = () => {
                    setCheckMark(!checked)
                }
                Completed: {product.isCompleted ? "Yes" : "No" */}



/* 
                <Checkbox
                onPress={() => setCheckMark(!checked)}
                title="Done"
                isChecked={checked}>
                {product.isCompleted ? "Yes" : "No"}
            </Checkbox> */

export default SelectedShoppingScreen; 