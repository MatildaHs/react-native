import { Text, View, ImageBackground, StatusBar, StyleSheet, Dimensions, NativeEventEmitter, Pressable } from 'react-native'
import StartingButton from '../components/StartingButton'
import { useState, useEffect } from 'react';
import Header from '../components/Header'
import { AntDesign } from '@expo/vector-icons';

import ProductInput from '../components/ProductInput';
import ShoppingList from '../components/ShoppingList';

import { findAll, dropTable } from '../database/DbUtils'

const StartingScreen = ({ navigation }) => {


    const [products, setProducts] = useState([])



    const handlePress = () => {
        navigation.navigate('MainShoppingScreen')
    }


    const emitter = new NativeEventEmitter()



    const deleteListener = emitter.addListener('delete', (productName) => {
        findAll()
            .then(res = () => {
                setProducts(res)
            })
            .catch(err => console.log(err))
    })

    useEffect(() => {
        findAll()
            .then(res => setProducts(res))
        return () => deleteListener.remove()
    }, [])

    return (
        <View style={styles.container}>
            <ImageBackground
                source={require('../assets/startlist.jpg')}
                resizeMode='cover'
                style={styles.imagebackground}
            >
                <Header />

                <ProductInput
                    setProducts={setProducts}

                />
                <StartingButton
                    navigation={navigation}
                />


                <StatusBar style="auto" />
            </ImageBackground>

        </View>
    )

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#DCDCDC',
        alignItems: 'center',

        justifyContent: "center"
    },
    imagebackground: {
        height: Dimensions.get('window').height,
        width: Dimensions.get('window').width
    },
    startbutton: {
        backgroundColor: '#3acb8d',
        width: "70%",
        height: 80,
        padding: 15,
        //marginVertical: "50%",
        alignSelf: "center",
        borderRadius: 20,
        justifyContent: "center"
    },
    buttontitle: {
        fontSize: 40,

        color: '#FFF'
    }
});

export default StartingScreen;