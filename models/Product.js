export default class Product {
    constructor(id, title, isCompleted, amount) {
        this.id = id;
        this.title = title;
        this.isCompleted = isCompleted;
        this.amount = amount;
    }

}